all: bin/test bin/texte bin/graphique

bin/graphique: obj/mainGraphique.o obj/JeuModeGraphique.o obj/Jeu.o obj/Terrain.o obj/Tour.o obj/Obstacle.o obj/Particule.o obj/Personnage.o obj/Vecteur2D.o obj/SDLSprite.o
	g++ -g -ggdb -Wall obj/mainGraphique.o obj/JeuModeGraphique.o obj/Jeu.o obj/Terrain.o obj/Tour.o obj/Obstacle.o obj/Particule.o obj/Personnage.o obj/Vecteur2D.o obj/SDLSprite.o -o bin/graphique -lSDL2 -lSDL2_ttf -lSDL2_image -lGL

bin/texte: obj/mainTexte.o obj/JeuModeTexte.o obj/winTxt.o obj/Jeu.o obj/Terrain.o obj/Tour.o obj/Obstacle.o obj/Particule.o obj/Personnage.o obj/Vecteur2D.o
	g++ -g -ggdb -Wall obj/mainTexte.o obj/JeuModeTexte.o obj/winTxt.o obj/Jeu.o obj/Terrain.o obj/Tour.o obj/Obstacle.o obj/Particule.o obj/Personnage.o obj/Vecteur2D.o -o bin/texte

bin/test: obj/mainTest.o obj/Jeu.o obj/Obstacle.o obj/Terrain.o obj/Tour.o obj/Particule.o obj/Vecteur2D.o obj/Personnage.o
	g++ -g -ggdb -Wall obj/mainTest.o obj/Jeu.o obj/Obstacle.o obj/Terrain.o obj/Tour.o obj/Particule.o obj/Vecteur2D.o obj/Personnage.o -o bin/test

obj/mainGraphique.o: src/mainGraphique.cpp src/JeuModeGraphique.h
	g++ -g -ggdb -Wall -c src/mainGraphique.cpp -o obj/mainGraphique.o

obj/mainTest.o: src/mainTest.cpp src/Jeu.h src/Vecteur2D.h src/Tour.h src/Personnage.h src/Terrain.h
	g++ -g -ggdb -Wall -c src/mainTest.cpp -o obj/mainTest.o

obj/mainTexte.o: src/mainTexte.cpp src/JeuModeTexte.h
	g++ -g -ggdb -Wall -c src/mainTexte.cpp -o obj/mainTexte.o

obj/winTxt.o: src/winTxt.cpp src/winTxt.h
	g++ -g -ggdb -Wall -c src/winTxt.cpp -o obj/winTxt.o

obj/JeuModeGraphique.o: src/JeuModeGraphique.cpp src/JeuModeGraphique.h src/Jeu.h src/SDLSprite.h
	g++ -g -ggdb -Wall -c src/JeuModeGraphique.cpp -o obj/JeuModeGraphique.o

obj/JeuModeTexte.o: src/JeuModeTexte.cpp src/JeuModeTexte.h src/Jeu.h src/winTxt.h
	g++ -g -ggdb -Wall -c src/JeuModeTexte.cpp -o obj/JeuModeTexte.o

obj/Jeu.o: src/Jeu.cpp src/Jeu.h src/Personnage.h src/Terrain.h src/Tour.h src/Obstacle.h src/Particule.h
	g++ -g -ggdb -Wall -c src/Jeu.cpp -o obj/Jeu.o

obj/Obstacle.o: src/Obstacle.cpp src/Obstacle.h src/Personnage.h src/Vecteur2D.h
	g++ -g -ggdb -Wall -c src/Obstacle.cpp -o obj/Obstacle.o

obj/Terrain.o: src/Terrain.cpp src/Terrain.h src/Tour.h src/Obstacle.h src/Vecteur2D.h
	g++ -g -ggdb -Wall -c src/Terrain.cpp -o obj/Terrain.o

obj/Tour.o: src/Tour.cpp src/Tour.h src/Particule.h src/Personnage.h
	g++ -g -ggdb -Wall -c src/Tour.cpp -o obj/Tour.o

obj/Personnage.o: src/Personnage.cpp src/Personnage.h src/Particule.h src/Tour.h src/Obstacle.h
	g++ -g -ggdb -Wall -c src/Personnage.cpp -o obj/Personnage.o

obj/Particule.o: src/Particule.cpp src/Particule.h src/Vecteur2D.h
	g++ -g -ggdb -Wall -c src/Particule.cpp -o obj/Particule.o

obj/Vecteur2D.o: src/Vecteur2D.cpp src/Vecteur2D.h
	g++ -g -ggdb -Wall -c src/Vecteur2D.cpp -o obj/Vecteur2D.o

obj/SDLSprite.o: src/SDLSprite.cpp src/SDLSprite.h
	g++ -g -ggdb -Wall -c src/SDLSprite.cpp -o obj/SDLSprite.o

clean:
	rm -f obj/*.o bin/*

doc: doc/doxyfile
	doxygen doc/doxyfile
