#ifndef _PARTICULE_H
#define _PARTICULE_H

#include "Vecteur2D.h"

/**
 * @brief Classe représentant une particule.
 */
class Particule
{
private:
    Vecteur2D Position; /**< Position de la particule. */
    double Rayon; /**< Rayon de la particule. */
public:
    /**
     * @brief Constructeur par défaut de la classe Particule.
     */
    Particule();
    
    /**
     * @brief Constructeur avec des paramètres pour initialiser la particule.
     * @param p Position initiale.
     * @param r Rayon initial.
     */
    Particule(const Vecteur2D& p, const double& r);
    
    /**
     * @brief Destructeur de la classe Particule.
     */
    ~Particule();
    
    /**
     * @brief Calcule la distance entre cette particule et une autre.
     * @param p La particule à laquelle calculer la distance.
     * @return La distance entre les deux particules.
     */
    double calculerDistance (const Particule& p);
    
    /**
     * @brief Getter pour obtenir la position de la particule.
     * @return La position de la particule.
     */
    Vecteur2D getPosition();
    
    /**
     * @brief Setter pour définir la position de la particule.
     * @param v La nouvelle position de la particule.
     */
    void setPosition(const Vecteur2D& v);
    
    /**
     * @brief Getter pour obtenir le rayon de la particule.
     * @return Le rayon de la particule.
     */
    double getRayon();
};

void testRegressionParticule();

#endif
