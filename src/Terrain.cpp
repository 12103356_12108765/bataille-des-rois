#include <iostream>
#include <fstream>
#include <cassert>
#include "Terrain.h"
using namespace std;

Terrain::Terrain() : dimx(20), dimy(40)
{
    // Ajout de tours
    Particule p1(Vecteur2D(5.0,35.0), 1.0);
    Tour tour1(p1, 10000, 50, 2, true, false, false);
    tabTour.push_back(tour1);

    Particule p2(Vecteur2D(10.0, 37.0), 2.0);
    Tour tour2(p2, 15000, 50, 3, true, true, false);
    tabTour.push_back(tour2);

    Particule p3(Vecteur2D(15.0, 35.0), 1.0);
    Tour tour3(p3, 10000, 50, 2, true, false, false);
    tabTour.push_back(tour3);

    // Création des tours opposées sur un terrain de dimensions 20.0 x 40.0
    Particule p1_opposee(Vecteur2D(20.0 - 5.0, 40.0 - 35.0), 1.0);  // Position miroir de p1
    Tour tour1_opposee(p1_opposee, 10000, 50, 2, true, false, true);
    tabTour.push_back(tour1_opposee);

    Particule p2_opposee(Vecteur2D(20.0 - 10.0, 40.0 - 37.0), 2.0);  // Position miroir de p2
    Tour tour2_opposee(p2_opposee, 10000, 50, 3, true, true, true);
    tabTour.push_back(tour2_opposee);

    Particule p3_opposee(Vecteur2D(20.0 - 15.0, 40.0 - 35.0), 1.0);  // Position miroir de p3
    Tour tour3_opposee(p3_opposee, 10000, 50, 2, true, false, true);
    tabTour.push_back(tour3_opposee);


    // Ajout d'obstacles
    Vecteur2D obs1_x(0.0, 0.0);
    Vecteur2D obs1_y(0.0, dimy-1);
    Obstacle obstacle1(obs1_x, obs1_y, false);
    tabObs.push_back(obstacle1);

    Vecteur2D obs2_x(dimx-1, 0.0);
    Vecteur2D obs2_y(dimx-1, dimy-1);
    Obstacle obstacle2(obs2_x, obs2_y, false);
    tabObs.push_back(obstacle2);

    // Nouvellement ajouté pour le haut et le bas
    Vecteur2D obs3_x(0.0, 0.0); // Coin supérieur gauche
    Vecteur2D obs3_y(dimx-1, 0.0); // Coin supérieur droit
    Obstacle obstacle3(obs3_x, obs3_y, false);
    tabObs.push_back(obstacle3);

    Vecteur2D obs4_x(0.0, dimy-1); // Coin inférieur gauche
    Vecteur2D obs4_y(dimx-1, dimy-1); // Coin inférieur droit
    Obstacle obstacle4(obs4_x, obs4_y, false);
    tabObs.push_back(obstacle4);

    // Obstacle 5 - De 0 à juste avant 1/3 de dimx, centré verticalement
    Vecteur2D obs5_x(0.0, (dimy/2)-1.0);
    Vecteur2D obs5_y((dimx/3) - 2.0, (dimy/2)+1.0);  // S'arrête juste avant le passage à 1/3 de dimx
    Obstacle obstacle5(obs5_x, obs5_y, false);
    tabObs.push_back(obstacle5);

    // Obstacle 6 - De juste après 1/3 de dimx à juste avant 2/3 de dimx, centré verticalement
    Vecteur2D obs6_x((dimx/3) + 2.0, (dimy/2)-1.0);  // Commence juste après le passage à 1/3 de dimx
    Vecteur2D obs6_y((2.0*dimx/3) - 2.0, (dimy/2)+1.0);  // S'arrête juste avant le passage à 2/3 de dimx
    Obstacle obstacle6(obs6_x, obs6_y, false);
    tabObs.push_back(obstacle6);

    // Obstacle 7 - De juste après 2/3 de dimx à la fin du terrain, centré verticalement
    Vecteur2D obs7_x((2.0*dimx/3) + 2.0, (dimy/2)-1.0);  // Commence juste après le passage à 2/3 de dimx
    Vecteur2D obs7_y(dimx, (dimy/2)+1.0);  // Va jusqu'à la fin du terrain
    Obstacle obstacle7(obs7_x, obs7_y, false);
    tabObs.push_back(obstacle7);

    srand(time(NULL));
    int nombreObstacles = 2; // 2 obstacles par camp
    for (int camp = 0; camp < 2; camp++) {
        int minY = (camp == 0) ? 2 : (dimy / 2) + 1;
        int maxY = (camp == 0) ? (dimy / 2) - 1 : dimy - 2;

        for (int i = 0; i < nombreObstacles; ++i) {
            int x1, y1, x2, y2;
            Vecteur2D positionDebut, positionFin;
            Obstacle nouvelObstacle;
            bool valide;

            do {
                x1 = 2 + rand() % (dimx - 4);
                y1 = minY + rand() % (maxY - minY + 1);
                x2 = x1 + rand() % ((dimx - x1) - 2);
                y2 = y1 + rand() % ((maxY - y1 + 1) - 2);
                positionDebut = Vecteur2D(x1, y1);
                positionFin = Vecteur2D(x2, y2);
                nouvelObstacle = Obstacle(positionDebut, positionFin, true);

                valide = true;
                for (unsigned int j = 0; j < tabObs.size(); j++) {
                    if (estTropProche(nouvelObstacle, tabObs[j]) && detecterCollisionObstacle(nouvelObstacle.getInf()) && detecterCollisionObstacle(nouvelObstacle.getSup())) {
                        valide = false;
                        break;
                    }
                }
            } while (!valide);

            tabObs.push_back(nouvelObstacle);
            //std::cout << "Obstacle placé de (" << x1 << ", " << y1 << ") à (" << x2 << ", " << y2 << ") dans le camp " << (camp + 1) << std::endl;
        }
    }



}

Terrain::Terrain(const Tour& tTour, const unsigned int& dx, const unsigned int& dy)
    : dimx(dx), dimy(dy) {
    for (int i = 0; i < 3; ++i) {
        tabTour[i] = tTour;
    }
}

Terrain:: ~Terrain () {
    dimx = dimy = 0;
    tabObs.clear();
    tabTour.clear();
}

bool Terrain::estTropProche(const Obstacle& a, const Obstacle& b) {
    // Une marge simple où aucun obstacle ne doit être à moins de 5 pixels d'un autre
    int marge = 5;
    return (abs(a.getInf().x - b.getInf().x) < marge && abs(a.getInf().y - b.getInf().y) < marge) ||
           (abs(a.getSup().x - b.getSup().x) < marge && abs(a.getSup().y - b.getSup().y) < marge);
}

bool Terrain::detecterCollisionObstacle(const Vecteur2D& position) 
{
    if (position.x < 0 || position.x >= dimx || position.y < 0 || position.y >= dimy) {
        return false;
    }
    for (unsigned int i = 0; i < tabTour.size(); i++) {
        double distanceX = abs(position.x - tabTour[i].getTour().getPosition().x);
        double distanceY = abs(position.y - tabTour[i].getTour().getPosition().y);
        double distanceCentres = sqrt(distanceX * distanceX + distanceY * distanceY);
        double distanceMinimale = tabTour[i].getTour().getRayon();

        if (distanceCentres < distanceMinimale) {
            return false;
        }
    }
    for (unsigned int i = 0; i < tabObs.size(); i++) {
        if (position.x >= tabObs[i].getInf().x && position.x <= tabObs[i].getSup().x &&
            position.y >= tabObs[i].getInf().y && position.y <= tabObs[i].getSup().y) 
                return false;
    }
    return true;
}


void Terrain::chargerTerrain(const string& ch) {
    ifstream fichier(ch);
    if (!fichier.is_open()) {
        cout << "Erreur : Impossible d'ouvrir le fichier " << ch << endl;
        return;
    }
    int nbTours = tabTour.size();
    int nbObs = tabObs.size();
    fichier >> dimx >> dimy >> nbTours >> nbObs;
    for (unsigned int i = 0; i < tabTour.size(); ++i) {
        double posX, posY;
        fichier >> posX >> posY;
        Vecteur2D position(posX, posY);
        Particule p(Vecteur2D(0.0, 0.0), 0.0);
        unsigned int vie, degats, rayonAttaque;
        bool enVie, principale, estJ;
        fichier >> vie >> degats >> rayonAttaque >> enVie >> principale >> estJ;
        Tour tour(p, vie, degats, rayonAttaque, enVie, principale, estJ);
        tabTour[i] = tour;
    }
    for (unsigned int i = 0; i < tabObs.size(); ++i) {
        Vecteur2D pX, pY;
        bool estPassable;
        fichier >> pX.x >> pX.y >> pY.x >> pY.y >> estPassable;
        tabObs.push_back(Obstacle(pX, pY, estPassable));
    }

    fichier.close();
}

void Terrain::sauverTerrain(const string& ch) {
    std::ofstream fichier(ch);
    if (!fichier.is_open()) {
        cout << "Erreur : Impossible de créer le fichier " << ch << endl;
        return;
    }
    fichier << dimx << " " << dimy << " " << tabTour.size() << " " << tabObs.size() << endl;
    for (unsigned int i = 0; i < tabObs.size(); ++i) {
        fichier << tabTour[i].getTour().getPosition().x << " " << tabTour[i].getTour().getPosition().y << " ";
        fichier << tabTour[i].getVie() << " " << tabTour[i].getDegats() << " " << tabTour[i].getRayonAttaque() << " ";
        fichier << tabTour[i].getEnVie() << " " << tabTour[i].getPrincipale() << " " << tabTour[i].getEstJ() << std::endl;
    }
    for (unsigned int i = 0; i < tabObs.size(); ++i) 
    {
        fichier << tabObs[i].getInf().x << " " << tabObs[i].getSup().y << " ";
        fichier << " " << tabObs[i].getEstPassable() << std::endl;
    }

    fichier.close();
}

vector<Tour>& Terrain::getTabTour() {
    return this->tabTour;
}

const vector<Tour>& Terrain::getTabTour() const {
    return this->tabTour;
}

vector<Obstacle> Terrain::getTabObs() const {
    return this->tabObs;
}


bool Terrain::estPositionValide(const Vecteur2D& position) {
    if (position.x < 0 || position.x >= dimx || position.y < 0 || position.y >= dimy) {
        return false;
    }
    for (unsigned int i = 0; i < tabTour.size(); i++) {
        double distanceX = abs(position.x - tabTour[i].getTour().getPosition().x);
        double distanceY = abs(position.y - tabTour[i].getTour().getPosition().y);
        double distanceCentres = sqrt(distanceX * distanceX + distanceY * distanceY);
        double distanceMinimale = tabTour[i].getTour().getRayon();

        if (distanceCentres < distanceMinimale) {
            return false;
        }
    }
    for (unsigned int i = 0; i < tabObs.size(); i++) {
        if (position.x >= tabObs[i].getInf().x && position.x <= tabObs[i].getSup().x &&
            position.y >= tabObs[i].getInf().y && position.y <= tabObs[i].getSup().y &&
            tabObs[i].getEstPassable()==false)  
                return false;
    }
    return true;
}

int Terrain::getDimx() const{
    return this->dimx;
}

int Terrain::getDimy() const{
    return this->dimy;
}

void testRegressionTerrain() {
    // Création d'un terrain
    Terrain t;
    assert(t.getDimx() == 20 && t.getDimy() == 40);
    cout << "Création du terrain OK\n";

    // Vérification de l'ajout initial des tours
    vector<Tour> tours = t.getTabTour();
    assert(tours.size() == 6);  // Vérifie le nombre de tours ajoutées initialement
    cout << "Ajout initial des tours OK\n";

    // Vérification des propriétés des tours
    assert(tours[0].getVie() == 10000 && tours[0].getDegats() == 50);
    cout << "Propriétés des tours vérifiées OK\n";

    // Vérification de l'ajout initial des obstacles
    vector<Obstacle> obstacles = t.getTabObs();
    assert(obstacles.size() == 11);  // Compte les obstacles ajoutés initialement
    cout << "Ajout initial des obstacles OK\n";

    // Test de la méthode estPositionValide
    assert(t.estPositionValide(Vecteur2D(5.0, 5.0)) == true);  // Devrait être valide
    assert(t.estPositionValide(Vecteur2D(-1.0, 5.0)) == false);  // En dehors des limites du terrain
    assert(t.estPositionValide(Vecteur2D(10.0, 37.0)) == false);  // Sur une tour
    cout << "Vérification des positions valides et invalides OK\n";

    cout << "Tous les tests passés avec succès !\n";
}
