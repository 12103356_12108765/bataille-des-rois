#include "Obstacle.h"
#include <iostream>
#include <cassert>
using namespace std;

Obstacle::Obstacle() : estPassable(false) {}

Obstacle::Obstacle(const Vecteur2D& pX, const Vecteur2D& pY, const bool& estPsbl) : inf(pX), sup(pY), estPassable(estPsbl) {}

Obstacle::~Obstacle () {}

bool Obstacle::getEstPassable() const {
    return estPassable;
}

Vecteur2D Obstacle::getInf() const {
    return inf;
}

Vecteur2D Obstacle::getSup() const {
    return sup;
}

void testRegressionObstacle() {
    // Test du constructeur par défaut
    Obstacle o1;
    assert(o1.getEstPassable() == false);
    std::cout << "Constructeur par défaut OK\n";

    // Test du constructeur paramétrique
    Vecteur2D inf(0.0, 0.0);
    Vecteur2D sup(10.0, 10.0);
    bool estPassable = true;
    Obstacle o2(inf, sup, estPassable);
    assert(o2.getEstPassable() == true);
    assert(o2.getInf().x == 0.0 && o2.getInf().y == 0.0);
    assert(o2.getSup().x == 10.0 && o2.getSup().y == 10.0);
    std::cout << "Constructeur paramétrique OK\n";

    std::cout << "Tous les tests passés avec succès !\n";
}