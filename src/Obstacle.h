#ifndef _OBSTACLE_H
#define _OBSTACLE_H

#include "Vecteur2D.h"

/**
 * @class Obstacle
 * @brief Classe représentant un obstacle dans le jeu.
 */
class Obstacle
{
private:
    Vecteur2D inf; /**< Vecteur représentant la position inferieur gauche de l'obstacle. */
    Vecteur2D sup; /**< Vecteur représentant la position superieur droite de l'obstacle. */
    bool estPassable; /**< Indique si l'obstacle est passable ou non. */

public:
    /**
     * @brief Constructeur par défaut.
     */
    Obstacle();

    /**
     * @brief Constructeur avec paramètres.
     * @param pX Vecteur représentant la position inferieur gauche de l'obstacle.
     * @param pY Vecteur représentant la position superieur droite de l'obstacle.
     * @param estPsbl Indique si l'obstacle est passable ou non.
     * @param mdVit Influence de l'obstacle sur la vitesse.
     */
    Obstacle(const Vecteur2D& pX, const Vecteur2D& pY, const bool& estPsbl);

    /**
     * @brief Destructeur de la classe.
     */
    ~Obstacle();

    /**
     * @brief Accesseur pour l'indicateur de passabilité de l'obstacle.
     * @return true si l'obstacle est passable, false sinon.
     */
    bool getEstPassable() const;

    /**
     * @brief Accesseur pour la position inferieur gauche de l'obstacle.
     * @return Vecteur représentant la position x de l'obstacle.
     */
    Vecteur2D getInf() const;

    /**
     * @brief Accesseur pour la position superieur droite de l'obstacle.
     * @return Vecteur représentant la position y de l'obstacle.
     */
    Vecteur2D getSup() const;
};

void testRegressionObstacle();

#endif
