#ifndef _TOUR_H
#define _TOUR_H

#include "Particule.h"

class Personnage;

/**
 * @brief Classe représentant une tour dans un jeu.
 */
class Tour
{
private:
    Particule tour; /**< Particule représentant la tour. */
    unsigned int vie; /**< Vie de la tour. */
    unsigned int degats; /**< Dégâts infligés par la tour. */
    unsigned int rayonAttaque; /**< Rayon d'attaque de la tour. */
    bool enVie; /**< Indique si la tour est en vie. */
    bool principale; /**< Indique si la tour est principale. */
    bool estJ;
public:
    /**
     * @brief Constructeur par défaut de la classe Tour.
     */
    Tour();
    
    /**
     * @brief Constructeur avec une particule pour initialiser la tour.
     * @param t La particule représentant la tour.
     */
    Tour(const Particule& t);
    
    /**
     * @brief Constructeur avec des paramètres pour initialiser la tour.
     * @param t La particule représentant la tour.
     * @param v La vie initiale de la tour.
     * @param d Les dégâts infligés par la tour.
     * @param rA Le rayon d'attaque de la tour.
     * @param eVie Indique si la tour est en vie.
     * @param princip Indique si la tour est principale.
     */
    Tour(const Particule& t, unsigned int v, unsigned int d, unsigned int rA, bool eVie, bool princip, bool eJ);
    
    /**
     * @brief Destructeur de la classe Tour.
     */
    ~Tour();
    
    /**
     * @brief Fonction pour faire subir des dégâts à la tour.
     * @param degatARecevoir Les dégâts à infliger à la tour.
     */
    void recevoirDegats(const unsigned int& degatARecevoir);
    
    /**
     * @brief Fonction pour attaquer un personnage.
     * @param p La référence vers le personnage à attaquer.
     */
    void attaquer(Personnage& p);
    
    /**
     * @brief Getter pour obtenir la particule représentant la tour.
     * @return La particule représentant la tour.
     */
    Particule getTour() const;
    
    /**
     * @brief Getter pour obtenir le rayon d'attaque de la tour.
     * @return Le rayon d'attaque de la tour.
     */
    unsigned int getRayonAttaque() const;
    
    /**
     * @brief Getter pour obtenir la vie de la tour.
     * @return La vie de la tour.
     */
    unsigned int getVie() const;
    
    /**
     * @brief Getter pour obtenir les dégâts infligés par la tour.
     * @return Les dégâts infligés par la tour.
     */
    unsigned int getDegats() const;
    
    /**
     * @brief Getter pour savoir si la tour est principale.
     * @return True si la tour est principale, False sinon.
     */
    bool getPrincipale() const;
    
    /**
     * @brief Getter pour savoir si la tour est en vie.
     * @return True si la tour est en vie, False sinon.
     */
    bool getEnVie() const;

    /**
     * @brief Définit la vie de la tour.
     * 
     * Cette fonction permet de mettre à jour la quantité de vie de la tour, qui peut être modifiée suite à des dégâts ou des soins.
     * 
     * @param nV La nouvelle valeur de vie à assigner à la tour.
     */
    void setVie(const unsigned int& nV);

    /**
     * @brief Définit si la tour est en vie.
     * 
     * Utilisé pour changer l'état de vie de la tour, par exemple, marquer la tour comme détruite lorsque sa vie atteint 0.
     * 
     * @param eV Booléen indiquant si la tour est en vie (true) ou non (false).
     */
    void setEnVie(const bool& eV);

    /**
     * @brief Renvoie si la tour est contrôlée par le joueur.
     * 
     * Cette méthode permet de savoir si la tour est gérée par le joueur (par opposition à être contrôlée par l'ordinateur ou un autre joueur dans un contexte multijoueur).
     * 
     * @return True si la tour est contrôlée par le joueur, False sinon.
     */
    bool getEstJ() const;
    

};

void testRegressionTour();

#endif
