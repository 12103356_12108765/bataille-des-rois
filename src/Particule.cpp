#include"Particule.h"
#include <cassert>
#include <cmath>
#include <iostream>

Particule::Particule() 
{
    this->Position = Vecteur2D(0.0,0.0);
    this->Rayon = 0.0;
}

Particule::Particule(const Vecteur2D& p, const double& r) : Position(p), Rayon(r) {}

Particule::~Particule() {}

double Particule::calculerDistance(const Particule& p)
{
    double r, dx, dy;
    dx = this->Position.x-p.Position.x;
    dy = this->Position.y-p.Position.y;
    r = sqrt(dx*dx+dy*dy);
    return r;
}

Vecteur2D Particule::getPosition()
{
    return this->Position;
}

void Particule::setPosition(const Vecteur2D& v)
{
    this->Position.x = v.x;
    this->Position.y = v.y;
}

double Particule::getRayon()
{
    return this->Rayon;
}


void testRegressionParticule() {
    // Test du constructeur par défaut
    Particule p1;
    assert(p1.getPosition().x == 0.0 && p1.getPosition().y == 0.0 && p1.getRayon() == 0.0);
    std::cout << "Constructeur par défaut OK\n";

    // Test du constructeur paramétrique
    Vecteur2D pos(1.0, 2.0);
    double rayon = 3.0;
    Particule p2(pos, rayon);
    assert(p2.getPosition().x == 1.0 && p2.getPosition().y == 2.0 && p2.getRayon() == 3.0);
    std::cout << "Constructeur paramétrique OK\n";

    // Test de la méthode setPosition
    Vecteur2D newPos(4.0, 5.0);
    p1.setPosition(newPos);
    assert(p1.getPosition().x == 4.0 && p1.getPosition().y == 5.0);
    std::cout << "Méthode setPosition OK\n";

    // Test de la méthode calculerDistance
    double expectedDistance = sqrt((newPos.x - pos.x) * (newPos.x - pos.x) + (newPos.y - pos.y) * (newPos.y - pos.y));
    double calculatedDistance = p1.calculerDistance(p2);
    assert(fabs(calculatedDistance - expectedDistance) < 1e-6);
    std::cout << "Méthode calculerDistance OK\n";

    std::cout << "Tous les tests passés avec succès !\n";
}