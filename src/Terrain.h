#ifndef _TERRAIN_H
#define _TERRAIN_H

#include <string>
#include "Tour.h"
#include "Obstacle.h"
#include "Vecteur2D.h"
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

/**
 * @class Terrain
 * @brief Représente un terrain de jeu.
 */
class Terrain
{
private:
    int dimx; /**< Largeur du terrain. */
    int dimy; /**< Hauteur du terrain. */
    vector<Tour> tabTour; /**< Vecteur contenant les tours du terrain. */
    vector<Obstacle> tabObs; /**< Vecteur contenant les obstacles du terrain. */

public:
    /**
     * @brief Constructeur par défaut.
     */
    Terrain();

    /**
     * @brief Constructeur prenant des paramètres spécifiques.
     * @param tTour Tour à utiliser pour initialiser le terrain.
     * @param dx Largeur du terrain.
     * @param dy Hauteur du terrain.
     */
    Terrain(const Tour& tTour, const unsigned int& dx, const unsigned int& dy);

    /**
     * @brief Destructeur de la classe Terrain.
     */
    ~Terrain();

    /**
     * @brief Charge un terrain à partir d'un fichier.
     * @param ch Chemin vers le fichier contenant les informations du terrain.
     */
    void chargerTerrain(const string& ch);

    /**
     * @brief Sauvegarde le terrain dans un fichier.
     * @param ch Chemin vers le fichier de sauvegarde.
     */
    void sauverTerrain(const string& ch);

    /**
     * @brief Vérifie si une position est valide sur le terrain.
     * @param position Position à vérifier.
     * @return Vrai si la position est valide, faux sinon.
     */
    bool estPositionValide(const Vecteur2D& position);

    /**
     * @brief Vérifie s'il y a une collision entre les obstacles du terrain.
     * @param position Position à vérifier.
     * @return Vrai s'il y a une collision, faux sinon.
     */
    bool detecterCollisionObstacle(const Vecteur2D& position);

    /**
     * @brief Récupère le vecteur contenant les tours du terrain.
     * @return Vecteur contenant les tours du terrain.
     */
    vector<Tour>& getTabTour();

    const vector<Tour>& getTabTour() const;

    /**
     * @brief Récupère le vecteur contenant les obstacles du terrain.
     * @return Vecteur contenant les obstacles du terrain.
     */
    vector<Obstacle> getTabObs() const;

    /**
     * @brief Récupère la largeur du terrain.
     * @return Largeur du terrain.
     */
    int getDimx() const;

    /**
     * @brief Récupère la hauteur du terrain.
     * @return Hauteur du terrain.
     */
    int getDimy() const;

    /**
     * @brief Vérifie si deux obstacles sont trop proches l'un de l'autre sur le terrain.
     *
     * Cette fonction détermine si la distance entre deux obstacles est inférieure à une certaine limite, ce qui pourrait indiquer un risque de collision ou d'interférence dans le placement.
     *
     * @param a Premier obstacle à comparer.
     * @param b Deuxième obstacle à comparer.
     * @return Vrai si les obstacles sont considérés comme étant trop proches, faux sinon.
     */
    bool estTropProche(const Obstacle& a, const Obstacle& b);
};

void testRegressionTerrain();

#endif
