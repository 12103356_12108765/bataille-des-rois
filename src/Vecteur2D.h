#ifndef _VECTEUR2D_H
#define _VECTEUR2D_H

#include <math.h>

/**
 * @struct Vecteur2D
 * @brief Structure représentant un vecteur 2D.
 */
struct Vecteur2D {
    double x; /**< Coordonnée x du vecteur. */
    double y; /**< Coordonnée y du vecteur. */

    /**
     * @brief Constructeur par défaut.
     */
    Vecteur2D();

    /**
     * @brief Constructeur prenant des coordonnées spécifiques.
     * @param nx Coordonnée x du vecteur.
     * @param ny Coordonnée y du vecteur.
     */
    Vecteur2D(const double& nx, const double& ny);

    /**
     * @brief Surcharge de l'opérateur d'addition.
     */
    Vecteur2D operator+(const Vecteur2D& a);

    /**
     * @brief Surcharge de l'opérateur de soustraction.
     */
    Vecteur2D operator-(const Vecteur2D& a);

    /**
     * @brief Surcharge de l'opérateur de multiplication par un scalaire.
     */
    Vecteur2D operator*(const double& f);

    /**
     * @brief Surcharge de l'opérateur de multiplication par composante.
     */
    Vecteur2D operator*(const Vecteur2D& a);

    /**
     * @brief Surcharge de l'opérateur d'égalité.
     */
    bool operator==(const Vecteur2D& a) const;

    /**
     * @brief Surcharge de l'opérateur de différence.
     */
    bool operator!=(const Vecteur2D& a) const;

    bool operator<=(const double& val) const;

    /**
     * @brief Calcule la norme du vecteur.
     */
    void calculNorme();

};

void testRegressionVecteur2D();

#endif
