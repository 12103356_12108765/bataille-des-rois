#include "Tour.h"
#include "Personnage.h"
#include <iostream>
#include <cassert>
using namespace std;

Tour::Tour() : tour(Vecteur2D(0.0, 0.0), 0.0), vie(1000), degats(50), rayonAttaque(20), enVie(true), principale(false), estJ(true) {}

Tour::Tour(const Particule& t) : tour(t), vie(1000), degats(50), rayonAttaque(20), enVie(true), principale(false), estJ(true) {}

Tour::Tour(const Particule& t, unsigned int v, unsigned int d, unsigned int rA, bool eVie, bool princip, bool eJ) : tour(t), vie(v), degats(d), rayonAttaque(rA), enVie(eVie), principale(princip), estJ(eJ) {}

Tour::~Tour(){}

void Tour::recevoirDegats(const unsigned int& degatARecevoir)
{
    if (this->enVie)  // S'assurer que la tour est en vie avant de traiter les dégâts
    {
        std::cout << "Tour avant attaque: Vie = " << this->vie << ", Dégâts reçus = " << degatARecevoir << std::endl;

        if (degatARecevoir >= this->vie) 
        {
            this->vie = 0;
            this->enVie = false;
            this->degats = 0;  // Optionnel: Réduire à zéro les dégâts de la tour si vous voulez qu'une tour détruite ne puisse plus attaquer.
            this->rayonAttaque = 0;  // Optionnel: Réduire le rayon d'attaque si nécessaire.
            std::cout << "Tour détruite." << std::endl;
        } 
        else 
        {
            this->vie -= degatARecevoir;
            std::cout << "Tour après attaque: Vie restante = " << this->vie << std::endl;
        }
    }
    else
    {
        std::cout << "Tentative d'attaque sur une tour déjà détruite." << std::endl;
    }

    assert(this->vie >= 0 && "La vie de la tour ne peut pas être négative.");
}



void Tour::attaquer(Personnage& p)
{
    p.recevoirDegats(this->degats);
}

Particule Tour::getTour() const
{
    return this->tour;
}

unsigned int Tour::getRayonAttaque() const
{
    return this->rayonAttaque;
}

unsigned int Tour::getVie() const
{ 
    return this->vie;
}

unsigned int Tour::getDegats() const
{
    return this->degats;
}

bool Tour::getPrincipale() const
{
    return this->principale;
}

bool Tour::getEnVie() const
{
    return this->enVie;
}

void Tour::setVie(const unsigned int& nV) {
    vie = nV;
}

void Tour::setEnVie(const bool& eV) {
    enVie = eV;
}

bool Tour::getEstJ() const {
    return estJ;
}


void testRegressionTour() {
    // Test du constructeur par défaut
    Tour t1;
    assert(t1.getVie() == 1000 && t1.getDegats() == 50);
    assert(t1.getRayonAttaque() == 20 && t1.getEnVie() == true);
    cout << "Constructeur par défaut OK\n";

    // Test du constructeur paramétrique
    Particule part(Vecteur2D(1.0, 1.0), 1.0);
    Tour t2(part, 500, 100, 30, true, true, true);
    assert(t2.getVie() == 500 && t2.getDegats() == 100);
    assert(t2.getRayonAttaque() == 30 && t2.getEnVie() == true);
    cout << "Constructeur paramétrique OK\n";

    // Test de la méthode recevoirDegats
    t2.recevoirDegats(100);
    assert(t2.getVie() == 400);
    cout << "Méthode recevoirDegats avec dégâts non mortels OK\n";

    // Test avec des dégâts mortels
    t2.recevoirDegats(500);
    assert(t2.getVie() == 0 && !t2.getEnVie());
    cout << "Méthode recevoirDegats avec dégâts mortels OK\n";

    // Test de la méthode attaquer
    Personnage p;
    unsigned int vieInitiale = p.getVie();
    t1.attaquer(p);
    assert(p.getVie() == vieInitiale - 50);  // Supposant que la vie initiale du personnage est supérieure à 50
    cout << "Méthode attaquer OK\n";

    cout << "Tous les tests passés avec succès !\n";
}