#include "Personnage.h"
#include <cassert>
#include <iostream>

Personnage::Personnage() : vie(1000),degats(50),rayonAttaque(50),coutP(1),enVie(true), estPlace(false),indiceChemin(0), compteurTicks(0), delaiTicksEntreDeplacements(10)
{
    if(rand()%2==0)
        estJ=false;
    else estJ=true;

    Particule p;
    perso = p;
}

Personnage::Personnage(const Particule& p, const int& v, const unsigned int& d, const unsigned int& rA, const unsigned int& cP, const bool& vivant, const bool& place, const bool& eJ) : 
perso(p), vie(v), degats(d), rayonAttaque(rA), coutP(cP), enVie(vivant), estPlace(place), estJ(eJ), compteurTicks(0), delaiTicksEntreDeplacements(10) {}

Personnage::~Personnage() {}

void Personnage::recevoirDegats(const unsigned int& degatARecevoir) {
    std::cout << "Personnage reçoit " << degatARecevoir << " dégâts, vie avant attaque: " << vie << "\n";
    if (enVie) {
        vie -= degatARecevoir;
        if (vie <= 0) {
            vie = 0;
            enVie = false;
            degats = 0;
            rayonAttaque = 0.0;
            estPlace = false;
            std::cout << "Personnage est détruit.\n";
        }
    }
}

void Personnage::attaquer(Tour& t)
{
    t.recevoirDegats(this->degats);
}

void Personnage::attaquer(Personnage& p)
{
    p.recevoirDegats(this->degats);
}

Particule Personnage::getPerso() const
{
    return this->perso;
}

int Personnage::getVie() const
{
    return this->vie;
}

unsigned int Personnage::getCout() const
{
    return this->coutP;
}

unsigned int Personnage::getDegats() const
{
    return this->degats;
}

bool Personnage::getEstPlace() const
{
    return this->estPlace;
}
unsigned int Personnage::getRayonAttaque() const 
{
    return this->rayonAttaque;
}

void Personnage::gauche(Terrain& t)
{
    Vecteur2D nouvellePos = perso.getPosition();
    nouvellePos.x--; // Déplace à gauche
    if(t.estPositionValide(nouvellePos)) // Vérifie si la nouvelle position est valide
    {
        perso.setPosition(nouvellePos); // Met à jour la position si valide
    }
}

void Personnage::droite(Terrain& t)
{
    Vecteur2D nouvellePos = perso.getPosition();
    nouvellePos.x++; // Déplace à droite
    if(t.estPositionValide(nouvellePos)) // Vérifie si la nouvelle position est valide
    {
        perso.setPosition(nouvellePos); // Met à jour la position si valide
    }
}

void Personnage::haut(Terrain& t)
{
    Vecteur2D nouvellePos = perso.getPosition();
    nouvellePos.y--; // Déplace à gauche
    if(t.estPositionValide(nouvellePos)) // Vérifie si la nouvelle position est valide
    {
        perso.setPosition(nouvellePos); // Met à jour la position si valide
    }
}

void Personnage::bas(Terrain& t)
{
    Vecteur2D nouvellePos = perso.getPosition();
    nouvellePos.y++; // Déplace à gauche
    if(t.estPositionValide(nouvellePos)) // Vérifie si la nouvelle position est valide
    {
        perso.setPosition(nouvellePos); // Met à jour la position si valide
    }
}

void Personnage::setEstPlace(const bool& b)
{
    this->estPlace = b;
}

bool Personnage::getEnVie() const
{
    return enVie;
}

bool Personnage::deplacerPersonnageSurChemin(Terrain& t) {
    if (cheminP.empty()) {
        return false;
    }
    if (indiceChemin < cheminP.size()) {
        if (compteurTicks >= delaiTicksEntreDeplacements) {
            compteurTicks = 0; // Réinitialiser le compteur après chaque déplacement

            // Déplacer le personnage
            char direction = cheminP[indiceChemin++];
            //std::cout << "Direction " << indiceChemin << " : " << direction << std::endl;
            switch(direction) {
                case 'g': gauche(t); break;
                case 'd': droite(t); break;
                case 'h': haut(t); break;
                case 'b': bas(t); break;
                default: break;
            }
        } else {
            compteurTicks++; // Incrémenter le compteur
        }
    } else {
        indiceChemin = 0; // Réinitialiser l'indice du chemin une fois tous les mouvements effectués
    }
    return !cheminP.empty();
}

void Personnage::setChemin(const vector<char>& chem) {
    cheminP = chem;
}

bool Personnage::getEstJ() const {
    return estJ;
}

vector<char> Personnage::getChemin() {
    return cheminP;
}

void testRegressionPersonnage() {
    // Test du constructeur par défaut
    Personnage p1;
    assert(p1.getVie() == 1000 && p1.getDegats() == 50);
    assert(p1.getEstPlace() == false && p1.getEnVie() == true);
    std::cout << "Constructeur par défaut OK\n";

    // Test du constructeur paramétrique
    Particule part(Vecteur2D(5.0, 5.0), 1.0);
    Personnage p2(part, 800, 75, 30, 2, true, true, true);
    assert(p2.getVie() == 800 && p2.getDegats() == 75);
    assert(p2.getRayonAttaque() == 30 && p2.getCout() == 2);
    assert(p2.getEstPlace() == true && p2.getEnVie() == true);
    std::cout << "Constructeur paramétrique OK\n";

    // Test de la méthode recevoirDegats
    p2.recevoirDegats(50);
    assert(p2.getVie() == 750);
    std::cout << "Méthode recevoirDegats OK\n";

    // Test de l'attribut enVie après réception de dégâts mortels
    p2.recevoirDegats(800); // Cela devrait tuer le personnage
    assert(p2.getVie() == 0 && !p2.getEnVie());
    std::cout << "Test dégâts mortels OK\n";

    // Test des méthodes de déplacement (fonctionnellement limitées sans instance de Terrain)
    std::cout << "Tests de déplacement non exécutables sans instance valide de Terrain\n";

    std::cout << "Tous les tests passés avec succès !\n";
}
