

#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"

#include "Jeu.h"
#include "JeuModeTexte.h"

JeuModeTexte::~JeuModeTexte() {}

void JeuModeTexte::afficherGagnerouPerdu()
{
    
    if (this->jeu.verifierFinPartie() == true)
    {
        if (this->jeu.getTerrain().getTabTour().size()==0) 
        {
            std::cout << "Malheureusement, tu as PERDU."<<std::endl;
        }
        else
        {
            std::cout << "Tu as GAGNE."<< std::endl;
        }
    }
}

void JeuModeTexte::afficherMenuPrincipal()
{
    termClear();
    char choix ;
    bool boucle = true;
    while (boucle) {
        std::cout << "Menu Principal" << std::endl;
        std::cout << "---------------" << std::endl;
        std::cout << "1. Jouer" << std::endl;
        std::cout << "2. Info Personnage" << std::endl;
        std::cout << "3. Aide : comment jouer" << std::endl;

        std::cout << "Entrez votre choix : ";
        cin >> choix;

        switch (choix)
        {
            case '1':
                std::cout << std::endl << "Lancement du jeu Bataille des Rois" << std::endl << std::endl;
                boucle = false;
                termClear();
                afftxtboucleJeu();
                break; 
            case '2':
                std::cout << std::endl << "Affichage des informations sur les personnages..." << std::endl << std::endl;
                for(unsigned int i = 0; i < 8; i++)
                {
                    std::cout << "Personnage " << i << std::endl;
                    std::cout << "Vie: " << jeu.getTabPersonnageEnJeu()[i].getVie() << std::endl;
                    std::cout << "Dégâts: " << jeu.getTabPersonnageEnJeu()[i].getDegats() << std::endl;
                    std::cout << "Rayon d'attaque: " << jeu.getTabPersonnageEnJeu()[i].getRayonAttaque() << std::endl;
                    std::cout << "Coût en ressources: " << jeu.getTabPersonnageEnJeu()[i].getCout() << std::endl;
                }

                break;
            case '3':
                std::cout << std::endl << "Le jeu Bataille des Rois est un jeu de stratégie en temps réel où un joueur affronte une mini" << std::endl
                << "intelligence artificielle. Le joueur contrôle un roi et doit détruire la tour de son adversaire tout" << std::endl
                << "en défendant la sienne. Le joueur dispose d'un deck de cartes représentant différentes unités et" << std::endl
                << "sorts qu'il peut invoquer sur le champ de bataille pour attaquer et défendre." << std::endl;

                std::cout << std::endl << "Il faut sélectionner une carte parmi les 4 proposées et la placer dans votre camp sur le terrain avec les touches z, q, s, d du clavier, puis appuyer sur f pour la placer." << std::endl;

                
                break;
            default:
                std::cout << std::endl << "Choix invalide. Veuillez sélectionner une option valide." << std::endl << std::endl;
                break;
        }
    }
}

JeuModeTexte::JeuModeTexte() {}

void JeuModeTexte::affTexte(WinTXT &win, const Jeu &jeu)
{
    const Terrain &ter = jeu.getTerrain();
	for (int i = 0; i < ter.getDimx(); i++)
    {
        for (int j = 0; j < ter.getDimy(); j++)
        {
             win.print(i,j,'.');
        }
    }
    // affichage des tours 
    bool principaleDetruite = false;

    // Vérifier d'abord si la tour principale est détruite
    for (unsigned int i = 0; i < ter.getTabTour().size(); i++) {
        if (ter.getTabTour()[i].getPrincipale() && !ter.getTabTour()[i].getEnVie()) 
        {
            principaleDetruite = true;
            break;
        }
    }

    // Si la tour principale est détruite, ne rien afficher
    if (!principaleDetruite) {
        for (unsigned int i = 0; i < ter.getTabTour().size(); i++) {
            if (ter.getTabTour()[i].getEnVie()) 
            {
                win.print(ter.getTabTour()[i].getTour().getPosition().x, ter.getTabTour()[i].getTour().getPosition().y, 'T');
            }
        }
    }

    for (unsigned int i = 0; i < 8; i++)
    {
        char indice = '0' + i;
        if(jeu.getTabPersonnageEnJeu()[i].getEnVie())
            win.print(jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().x, jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().y, indice);
    }

    // affichage des obstacles
    for (unsigned int i = 0; i < ter.getTabObs().size(); i++) {
        const Vecteur2D& inf = ter.getTabObs()[i].getInf();
        const Vecteur2D& sup = ter.getTabObs()[i].getSup();
        char symbole = ter.getTabObs()[i].getEstPassable() ? 'S' : '$'; // 'S' pour passable, '$' pour non passable
        for (int x = inf.x; x <= sup.x; x++) {
            for (int y = inf.y; y <= sup.y; y++) 
            {
                win.print(x, y, symbole);
            }
        }
    }
    
    win.print(0, jeu.getTerrain().getDimy()+2, 'L');
    win.print(1, jeu.getTerrain().getDimy()+2, 'i');
    win.print(2, jeu.getTerrain().getDimy()+2, 't');
    win.print(3, jeu.getTerrain().getDimy()+2, 'r');
    win.print(4, jeu.getTerrain().getDimy()+2, 'e');
    win.print(5, jeu.getTerrain().getDimy()+2, ' ');
    win.print(6, jeu.getTerrain().getDimy()+2, 'd');
    win.print(7, jeu.getTerrain().getDimy()+2, "'");
    win.print(8, jeu.getTerrain().getDimy()+2, 'e');
    win.print(9, jeu.getTerrain().getDimy()+2, 's');
    win.print(10, jeu.getTerrain().getDimy()+2, 's');
    win.print(11, jeu.getTerrain().getDimy()+2, 'e');
    win.print(12, jeu.getTerrain().getDimy()+2, 'n');
    win.print(13, jeu.getTerrain().getDimy()+2, 'c');
    win.print(14, jeu.getTerrain().getDimy()+2, 'e');
    win.print(15, jeu.getTerrain().getDimy()+2, ' ');
    win.print(16, jeu.getTerrain().getDimy()+2, ':');

    win.print(0, jeu.getTerrain().getDimy()+3, '[');
    for (unsigned int i = 0; i < jeu.getEssenceJ(); i++)
    {
        win.print(i+1, jeu.getTerrain().getDimy()+3, '=');
    }
    win.print(12, jeu.getTerrain().getDimy()+3, ']');
    char nbEssence = jeu.getEssenceJ() + '0';
    win.print(14, jeu.getTerrain().getDimy()+3, nbEssence);
    win.draw();
}

void JeuModeTexte::afftxtboucleJeu()
{
    termClear();
    srand(time(NULL));
    // Création d'une nouvelle fenêtre en mode texte avec les dimensions du terrain
    WinTXT win(jeu.getTerrain().getDimx(), jeu.getTerrain().getDimy()+4);
    bool ok = true;
    char c;
    int personnageActuel = 0;

    do {
        // Efface la fenêtre avant d'afficher à nouveau le jeu
        win.clear();
        
        // Appel de la fonction d'affichage du jeu
        affTexte(win, jeu);

        usleep(50000);

        // Exemple de traitement des actions automatiques du jeu
        jeu.actionAutomatique();
        // Récupération de la touche pressée par l'utilisateur

        for (int i = 4; i < 8; i++) {
            if (!jeu.getTabPersonnageEnJeu()[i].getEstPlace()) { // S'assurer que le personnage peut se déplacer
                int deplacementX = 1 + rand() % (jeu.getTerrain().getDimx() - 2);
                int deplacementY =  jeu.getTerrain().getDimy()/2 + rand() % (jeu.getTerrain().getDimy()/2 - 1);

                // Correction pour éviter de sortir des limites du terrain
                Vecteur2D pos = jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition();
                deplacementX = std::min(deplacementX,(int) (jeu.getTerrain().getDimx() - pos.x - 1));
                deplacementY = std::min(deplacementY, (int) (jeu.getTerrain().getDimy() - pos.y - 1));

                for (int j = 0; j < deplacementX; j++) {
                    jeu.actionClavier('d', i);
                }
                for (int j = 0; j < deplacementY; j++) {
                    jeu.actionClavier('b', i);
                }
                jeu.placerPersonnageDansFile(i);
            }
        }


        c = win.getCh();
        
         switch (c) {
            case '0': case '1': case '2': case '3':
                personnageActuel = c - '0';  // Convertit le caractère en entier
                for(double i = 0; i < jeu.getTerrain().getDimx()/2; i++) {
                    jeu.actionClavier('d', personnageActuel);
                }
                for(double i = 0; i < jeu.getTerrain().getDimy()/4; i++) {
                    jeu.actionClavier('h', personnageActuel);
                }
                break;
            case 'z':  // Haut
                if (!jeu.getTabPersonnageEnJeu()[personnageActuel].getEstPlace()) {
                    jeu.actionClavier('h', personnageActuel);
                }
                break;
            case 'q':  // Gauche
                if (!jeu.getTabPersonnageEnJeu()[personnageActuel].getEstPlace()) {
                    jeu.actionClavier('g', personnageActuel);
                }
                break;
            case 's':  // Bas
                if (!jeu.getTabPersonnageEnJeu()[personnageActuel].getEstPlace()) {
                    jeu.actionClavier('b', personnageActuel);
                }
                break;
            case 'd':  // Droite
                if (!jeu.getTabPersonnageEnJeu()[personnageActuel].getEstPlace()) {
                    jeu.actionClavier('d', personnageActuel);
                }
                break;
            case 'f':  // Placer personnage
                jeu.placerPersonnageDansFile(personnageActuel);
                break;
            case 'g':  // Quitter
                win.pause();
                break;
            case 'e':  // Quitter
                ok = false;
                break;
            default:
                // Gestion d'une touche non reconnue
                break;
        }
        // Rafraîchir l'affichage pour montrer les mises à jour de position
        win.clear();
    } while (ok);
    termClear();

}





