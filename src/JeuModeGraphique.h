#ifndef _JEUMODEGRAPHIQUE_H
#define _JEUMODEGRAPHIQUE_H

#include "Jeu.h"
#include "SDLSprite.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

/**
 * @file JeuModeGraphique.h
 * @brief Déclare la classe JeuModeGraphique qui gère l'interface graphique du jeu.
 */

/**
 * @class JeuModeGraphique
 * @brief Classe qui gère l'affichage graphique et les interactions utilisateur dans le jeu.
 *
 * Utilise la bibliothèque SDL pour créer une fenêtre, gérer les événements de la souris et du clavier, et afficher des éléments graphiques tels que les sprites et le texte.
 */
class JeuModeGraphique
{
    private:
        Jeu jeu; /**< Instance du jeu permettant de manipuler la logique du jeu. */
        SDL_Window *window; /**< Fenêtre principale du jeu. */
        SDL_Renderer *renderer; /**< Renderer associé à la fenêtre pour dessiner les graphiques. */
        TTF_Font *font; /**< Police de caractères utilisée pour afficher le texte. */
        SDLSprite font_im; /**< Sprite pour afficher des images basées sur des textes. */
        SDL_Color font_color; /**< Couleur du texte utilisé dans le jeu. */
        bool souris; /**< Indicateur si l'interaction avec la souris est active. */
        bool touche; /**< Indicateur si l'interaction avec le clavier est active. */
    public:
        JeuModeGraphique(); /**< Constructeur de JeuModeGraphique. */
        ~JeuModeGraphique(); /**< Destructeur de JeuModeGraphique. */
        
        void affGraphique(); /**< Met à jour et affiche les éléments graphiques du jeu. */
        void affBoucleJeu(); /**< Boucle principale du jeu gérant les mises à jour et l'affichage. */
        void afficherEcranTitre(); /**< Affiche l'écran titre du jeu. */
        void afficherInterfaceJeu(); /**< Affiche l'interface utilisateur du jeu. */
        void afficherVictoire(); /**< Affiche l'écran de victoire lorsque le joueur gagne. */
        void afficherDefaite(); /**< Affiche l'écran de défaite lorsque le joueur perd. */
        
        /**
         * @brief Dessine un cercle sur le renderer donné.
         * 
         * @param renderer Renderer sur lequel le cercle sera dessiné.
         * @param centreX Coordonnée X du centre du cercle.
         * @param centreY Coordonnée Y du centre du cercle.
         * @param radius Rayon du cercle.
         * @param color Couleur du cercle.
         */
        void DrawCircle(SDL_Renderer* renderer, int centreX, int centreY, int radius, SDL_Color color);
};

#endif
