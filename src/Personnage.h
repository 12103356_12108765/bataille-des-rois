#ifndef _PERSONNAGE_H
#define _PERSONNAGE_H

#include "Particule.h"
#include "Tour.h"
#include "Obstacle.h"
#include "Terrain.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>

/**
 * @class Personnage
 * @brief Classe représentant un personnage dans le jeu.
 */
class Personnage
{
private:
    Particule perso; /**< Particule représentant le personnage. */
    int vie; /**< Points de vie du personnage. */
    unsigned int degats; /**< Dégâts infligés par le personnage. */
    unsigned int rayonAttaque; /**< Rayon d'attaque du personnage. */
    unsigned int coutP; /**< Coût en ressources du personnage. */
    bool enVie; /**< Indique si le personnage est en vie ou non. */
    bool estPlace; /**< Indique si le personnage est placé sur le terrain ou non. */
    bool estJ;
    vector<char> cheminP;
    unsigned int indiceChemin = 0;
    int compteurTicks;
    int delaiTicksEntreDeplacements = 1;


public:
    /**
     * @brief Constructeur par défaut.
     */
    Personnage();

    /**
     * @brief Constructeur avec paramètres.
     * @param p Particule représentant le personnage.
     * @param v Points de vie du personnage.
     * @param d Dégâts infligés par le personnage.
     * @param rA Rayon d'attaque du personnage.
     * @param vivant Indique si le personnage est en vie ou non.
     * @param place Indique si le personnage est placé sur le terrain ou non.
     */
    Personnage(const Particule& p, const int& v, const unsigned int& d, const unsigned int& rA, const unsigned int& cP, const bool& vivant, const bool& place, const bool& eJ);

    /**
     * @brief Destructeur de la classe.
     */
    ~Personnage();

    /**
     * @brief Fonction permettant au personnage de recevoir des dégâts.
     * @param degatARecevoir Dégâts à recevoir.
     */
    void recevoirDegats(const unsigned int& degatARecevoir);

    /**
     * @brief Fonction permettant au personnage d'attaquer une tour.
     * @param t Tour à attaquer.
     */
    void attaquer(Tour& t);

    /**
     * @brief Fonction permettant au personnage d'attaquer un autre personnage.
     * @param p Personnage à attaquer.
     */
    void attaquer(Personnage& p);

    /**
     * @brief Accesseur pour la particule représentant le personnage.
     * @return Particule représentant le personnage.
     */
    Particule getPerso() const;

    /**
     * @brief Accesseur pour les points de vie du personnage.
     * @return Points de vie du personnage.
     */
    int getVie() const;

    /**
     * @brief Accesseur pour le coût en ressources du personnage.
     * @return Coût en ressources du personnage.
     */
    unsigned int getCout() const;

    /**
     * @brief Accesseur pour l'indicateur de placement du personnage.
     * @return true si le personnage est placé sur le terrain, false sinon.
     */
    bool getEstPlace() const;

    /**
     * @brief Accesseur pour le rayon d'attaque du personnage.
     * @return Rayon d'attaque du personnage.
     */
    unsigned int getRayonAttaque() const;

    /**
     * @brief Accesseur pour les dégâts infligés par le personnage.
     * @return Dégâts infligés par le personnage.
     */
    unsigned int getDegats() const;

    /**
     * @brief Déplace le personnage vers la gauche sur le terrain.
     * 
     * @param t Référence au terrain sur lequel le personnage se déplace.
     */
    void gauche(Terrain& t);

    /**
     * @brief Déplace le personnage vers la droite sur le terrain.
     * 
     * @param t Référence au terrain sur lequel le personnage se déplace.
     */
    void droite(Terrain& t);

    /**
     * @brief Déplace le personnage vers le haut sur le terrain.
     * 
     * @param t Référence au terrain sur lequel le personnage se déplace.
     */
    void haut(Terrain& t);

    /**
     * @brief Déplace le personnage vers le bas sur le terrain.
     * 
     * @param t Référence au terrain sur lequel le personnage se déplace.
     */
    void bas(Terrain& t);

    /**
     * @brief Définit si le personnage est placé sur le terrain.
     * 
     * Cette fonction met à jour l'état de placement du personnage, permettant de le marquer comme actif sur le terrain.
     * 
     * @param b État de placement du personnage (true si placé, false sinon).
     */
    void setEstPlace(const bool& b);

    /**
     * @brief Renvoie si le personnage est encore en vie.
     * 
     * @return true si le personnage est en vie, false s'il est mort.
     */
    bool getEnVie() const;

    /**
     * @brief Déplace le personnage selon le chemin précalculé.
     * 
     * Cette fonction fait avancer le personnage le long d'un chemin défini par des directions stockées dans un vecteur.
     * 
     * @param t Référence au terrain sur lequel le personnage se déplace.
     */
    bool deplacerPersonnageSurChemin(Terrain& t);

    /**
     * @brief Définit le chemin que le personnage doit suivre.
     * 
     * @param chem Vecteur de caractères représentant les directions du chemin à suivre.
     */
    void setChemin(const vector<char>& chem);

    /**
     * @brief Renvoie si le personnage est contrôlé par le joueur.
     * 
     * @return true si le personnage est contrôlé par le joueur, false s'il est contrôlé par l'ordinateur.
     */
    bool getEstJ() const;

    vector<char> getChemin();



};

void testRegressionPersonnage();

#endif
