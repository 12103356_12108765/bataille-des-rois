#ifndef _JEU_H
#define _JEU_H

#include "Terrain.h"
#include "Personnage.h"
#include <queue>
#include <chrono>
#include <thread>
#include <iostream>

using namespace std;

/**
 * @brief Classe représentant le jeu.
 */
class Jeu
{
private:
    queue<Personnage> fileAttentePersonnage; /**< File d'attente des personnages. */
    Personnage tabPersonnageEnJeu[8]; /**< Tableau des personnages en jeu. */
    queue<Personnage> fileAttentePersonnageEnnemi; /**< File d'attente des personnages ennemis. */
    Terrain terrain; /**< Terrain de jeu. */
    unsigned int nbEssenceJ; /**< Nombre d'essence disponible du joueur dans le jeu. */
    unsigned int nbEssenceE; /**< Nombre d'essence disponible de l'ennemi dans le jeu. */
    double temps; /**< Temps écoulé dans le jeu. */
    bool estPartieFinie; /**< Indique si la partie est terminée. */
    int compteurEssence;
    chrono::steady_clock::time_point debutPartie; 
public:
    /**
     * @brief Constructeur par défaut de la classe Jeu.
     */
    Jeu();
    
    /**
     * @brief Destructeur de la classe Jeu.
     */
    ~Jeu();
    
    /**
     * @brief Fonction pour effectuer une action automatique dans le jeu.
     */
    void actionAutomatique();
    
    /**
     * @brief Fonction pour effectuer une action en fonction des touches clavier dans le jeu.
     */
    void actionClavier(const char& c, const unsigned int& i);
    
    /**
     * @brief Fonction pour vérifier si la partie est terminée.
     * @return True si la partie est terminée, False sinon.
     */
    bool verifierFinPartie();
    
    /**
     * @brief Fonction pour terminer la partie.
     */
    void finirPartie();
    
    
    
    /**
     * @brief Fonction pour vérifier si un ennemi est proche d'un personnage.
     * @param p1 Le premier personnage.
     * @param p2 Le deuxième personnage.
     * @return True si un ennemi est proche, False sinon.
     */
    bool estEnnemiProche(const Personnage& p1, const Personnage& p2);
    
    /**
     * @brief Fonction pour vérifier si un ennemi est proche d'une tour.
     * @param t La tour.
     * @param p Le personnage ennemi.
     * @return True si un ennemi est proche de la tour, False sinon.
     */
    bool estEnnemiProche(const Tour& t, const Personnage& p);
    
    /**
     * @brief Getter pour obtenir le terrain de jeu.
     * @return Le terrain de jeu.
     */
    Terrain getTerrain() const;
    

    /**
     * @brief Génère le chemin optimal pour qu'un personnage puisse attaquer.
     * 
     * @param indicePersonnage Index du personnage pour lequel le chemin doit être généré.
     */
    void genererCheminOptimalPourAttaquer(int indicePersonnage);


    /**
     * @brief Obtient un pointeur vers le tableau des personnages en jeu.
     * 
     * @return Pointeur constant vers le tableau des personnages actuellement en jeu.
     */
    const Personnage* getTabPersonnageEnJeu() const;


    /**
     * @brief Place un personnage dans la file d'attente pour son tour.
     * 
     * @param index Index du personnage à ajouter à la file.
     */
    void placerPersonnageDansFile(const int& index);


    /**
     * @brief Retourne la quantité d'essence disponible pour le joueur.
     * 
     * @return Quantité d'essence actuellement disponible pour le joueur.
     */
    unsigned int getEssenceJ() const;


    /**
     * @brief Retourne la quantité d'essence disponible pour le joueur ennemi.
     * 
     * @return Quantité d'essence actuellement disponible pour le joueur ennemi.
     */
    unsigned int getEssenceE() const;

    double getTemps() const;
};

void testRegressionJeu();

#endif
