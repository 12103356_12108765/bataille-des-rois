#include "Vecteur2D.h"
#include <iostream>
#include <cassert>

Vecteur2D::Vecteur2D() : x(0.0), y(0.0) {}

Vecteur2D::Vecteur2D(const double& nx, const double& ny) : x(nx), y(ny) {}

Vecteur2D Vecteur2D::operator+(const Vecteur2D& a)
{
    Vecteur2D r;
	r.x = this->x + a.x;
	r.y = this->y + a.y;
    return r;
}

Vecteur2D Vecteur2D::operator-(const Vecteur2D& a)
{
	Vecteur2D r;
    r.x = this->x - a.x;
	r.y = this->y - a.y;
    return r;
}

Vecteur2D Vecteur2D::operator*(const Vecteur2D& a)
{
    Vecteur2D r;
    r.x = this->x * a.x - this->y * a.y;
    r.y = this->y * a.x + this->x * a.y;
    return r;
}

Vecteur2D Vecteur2D::operator*(const double& f)
{
    Vecteur2D r;
    r.x = this->x * f;
    r.y = this->y * f;
    return r;
}

void Vecteur2D::calculNorme()
{
    double norme = sqrt(this->x * this->x + this->y * this->y);
    if (norme != 0.0)
    {
        this->x = this->x / norme;
       this->y = this->y / norme;
    }
}

bool Vecteur2D::operator==(const Vecteur2D& a) const 
{
    return x == a.x && y == a.y;
}

bool Vecteur2D::operator!=(const Vecteur2D& a) const 
{
    return x != a.x || y != a.y;
}

bool Vecteur2D::operator<=(const double& val) const 
{
    double norme = sqrt(x * x + y * y);
    return norme <= val;
}

void testRegressionVecteur2D() {
    // Test du constructeur par défaut
    Vecteur2D v;
    assert(v.x == 0.0 && v.y == 0.0);
    std::cout << "Constructeur par défaut OK\n";

    // Test du constructeur paramétrique
    Vecteur2D v1(1.0, 2.0);
    assert(v1.x == 1.0 && v1.y == 2.0);
    std::cout << "Constructeur paramétrique OK\n";

    // Test de l'opérateur d'addition
    Vecteur2D v2(3.0, 4.0);
    Vecteur2D sum = v1 + v2;
    assert(sum.x == 4.0 && sum.y == 6.0);
    std::cout << "Opérateur d'addition OK\n";

    // Test de l'opérateur de soustraction
    Vecteur2D diff = v2 - v1;
    assert(diff.x == 2.0 && diff.y == 2.0);
    std::cout << "Opérateur de soustraction OK\n";

    // Test de l'opérateur de multiplication scalaire
    Vecteur2D scaled = v2 * 2.0;
    assert(scaled.x == 6.0 && scaled.y == 8.0);
    std::cout << "Opérateur de multiplication scalaire OK\n";

    // Test de l'opérateur de multiplication vectorielle
    Vecteur2D prod = v1 * v2;
    assert(prod.x == -5.0 && prod.y == 10.0); // x = 1*3 - 2*4, y = 2*3 + 1*4
    std::cout << "Opérateur de multiplication vectorielle OK\n";

    // Test de la normalisation
    v1.calculNorme();
    double norm = sqrt(v1.x * v1.x + v1.y * v1.y);
    assert(fabs(norm - 1.0) < 1e-6); // La norme devrait être 1 après normalisation
    std::cout << "Méthode calculNorme OK\n";

    // Test de l'opérateur d'égalité
    assert(v == Vecteur2D(0.0, 0.0));
    std::cout << "Opérateur d'égalité OK\n";

    // Test de l'opérateur d'inégalité
    assert(v1 != v);
    std::cout << "Opérateur d'inégalité OK\n";

    std::cout << "Tous les tests passés avec succès !\n";
}