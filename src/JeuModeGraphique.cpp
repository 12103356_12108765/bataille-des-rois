#include "JeuModeGraphique.h"

const int TAILLE_SPRITE = 20;


void JeuModeGraphique::DrawCircle(SDL_Renderer *renderer, int x, int y, int radius, SDL_Color color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    for (int w = 0; w < radius * 2; w++)
    {
        for (int h = 0; h < radius * 2; h++)
        {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx*dx + dy*dy) <= (radius * radius))
            {
                SDL_RenderDrawPoint(renderer, x + dx, y + dy);
            }
        }
    }
}

JeuModeGraphique::JeuModeGraphique() : jeu()
{
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    if (TTF_Init() != 0)
    {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if (!(IMG_Init(imgFlags) & imgFlags))
    {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    int dimx, dimy;
    dimx = jeu.getTerrain().getDimx();
    dimy = jeu.getTerrain().getDimy();
    dimx = dimx * TAILLE_SPRITE;
    dimy = dimy * TAILLE_SPRITE;

    // Creation de la fenetre
    window = SDL_CreateWindow("Bataille Des Rois", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dimx, dimy, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == nullptr)
    {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // FONTS
    font = TTF_OpenFont("data/DejaVuSansCondensed.ttf", 14);
    if (font == nullptr)
        font = TTF_OpenFont("../data/DejaVuSansCondensed.ttf", 14);
    if (font == nullptr)
    {
        cout << "Failed to load DejaVuSansCondensed.ttf! SDL_TTF Error: " << TTF_GetError() << endl;
        SDL_Quit();
        exit(1);
    }
    font_color.r = 50;
    font_color.g = 50;
    font_color.b = 255;
    //font_im.setSurface(TTF_RenderText_Solid(font, "Bataille Des Rois", font_color));
    //font_im.loadFromCurrentSurface(renderer);
    
}


JeuModeGraphique::~JeuModeGraphique()
{
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void drawThickRectangle(SDL_Renderer* renderer, const SDL_Rect& rect, int thickness, SDL_Color color) {
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);

    // Dessiner les côtés horizontaux
    for (int i = 0; i < thickness; i++) {
        SDL_RenderDrawLine(renderer, rect.x, rect.y + i, rect.x + rect.w, rect.y + i); // Haut
        SDL_RenderDrawLine(renderer, rect.x, rect.y + rect.h - i, rect.x + rect.w, rect.y + rect.h - i); // Bas
    }

    // Dessiner les côtés verticaux
    for (int i = 0; i < thickness; i++) {
        SDL_RenderDrawLine(renderer, rect.x + i, rect.y, rect.x + i, rect.y + rect.h); // Gauche
        SDL_RenderDrawLine(renderer, rect.x + rect.w - i, rect.y, rect.x + rect.w - i, rect.y + rect.h); // Droite
    }
}

void JeuModeGraphique::affGraphique()
{
    // Remplir l'écran de blanc
    SDL_SetRenderDrawColor(renderer, 230, 240, 255, 255);
    SDL_RenderClear(renderer);

    const Terrain &ter = jeu.getTerrain();
    SDL_Rect fillRect;
    
	for (int x = 0; x < ter.getDimx(); x++)
    {
        for (int y = 0; y < ter.getDimy(); y++)
        {
            fillRect = { x* TAILLE_SPRITE, y* TAILLE_SPRITE, (x+1)* TAILLE_SPRITE, (y+1)* TAILLE_SPRITE};
            if(x%2==0 && y%2==0) {
                SDL_SetRenderDrawColor( renderer, 0x00, 0xFF, 0x00, 0xFF );
            }
            else {
                SDL_SetRenderDrawColor( renderer, 0x00, 0xAF, 0x00, 0xFF );
            }
            SDL_RenderFillRect( renderer, &fillRect );

            
        }
    }
    
    
    for (unsigned int i = 0; i < ter.getTabObs().size(); i++) {
        const Vecteur2D& inf = ter.getTabObs()[i].getInf();
        const Vecteur2D& sup = ter.getTabObs()[i].getSup();

        fillRect = {
            static_cast<int>(inf.x * TAILLE_SPRITE),
            static_cast<int>(inf.y * TAILLE_SPRITE),
            static_cast<int>((sup.x - inf.x + 1) * TAILLE_SPRITE),
            static_cast<int>((sup.y - inf.y + 1) * TAILLE_SPRITE)
        };

        if (ter.getTabObs()[i].getEstPassable()) {
            // Dessin des contours pour les obstacles passables
            //SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255); 
            //SDL_RenderFillRect(renderer, &fillRect);
        } else {
            // Remplissage pour les obstacles non passables
            SDL_SetRenderDrawColor(renderer, 128, 0, 128, 255); 
            SDL_RenderFillRect(renderer, &fillRect);
        }
    }


    for (unsigned int i = 0; i < ter.getTabTour().size(); i++)
    {
        if (ter.getTabTour()[i].getEnVie())
        {
            SDL_Color color;
            if (ter.getTabTour()[i].getEstJ()) // Si la tour est contrôlée par le joueur
            {
                color = {0xFF, 0x00, 0x00, 0xFF}; // Rouge
            }
            else // Si la tour est contrôlée par l'ennemi
            {
                color = {0x00, 0x00, 0xFF, 0xFF}; // Bleu
                
            }

            SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);

            int posX = ter.getTabTour()[i].getTour().getPosition().x * TAILLE_SPRITE;
            int posY = ter.getTabTour()[i].getTour().getPosition().y * TAILLE_SPRITE;
            int rayon = ter.getTabTour()[i].getTour().getRayon() * TAILLE_SPRITE;
            
            DrawCircle(renderer, posX, posY, rayon, color);

            // Rectangle autour du cercle (tour)
            SDL_Rect rect;
            rect.x = posX - rayon;
            rect.y = posY - rayon;
            rect.w = 2 * rayon;
            rect.h = 2 * rayon;
            drawThickRectangle(renderer, rect, 2, color);
            SDL_RenderDrawRect(renderer, &rect); // Dessiner le contour du rectangle
        }
    }

    // Pour les personnages
    for (unsigned int i = 0; i < 8; i++)
    {
        if (jeu.getTabPersonnageEnJeu()[i].getEnVie())
        {
            SDL_Color color;
            if (jeu.getTabPersonnageEnJeu()[i].getEstJ()) // Si le personnage est contrôlé par le joueur
            {
                color = {0x00, 0x00, 0xFF, 0xFF}; // Bleu
            }
            else // Si le personnage est contrôlé par l'ennemi
            {
                color = {0xFF, 0x00, 0x00, 0xFF}; // Rouge
            }

            SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
            int posX = jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().x * TAILLE_SPRITE;
            int posY = jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().y * TAILLE_SPRITE;
            int rayon = jeu.getTabPersonnageEnJeu()[i].getPerso().getRayon() * TAILLE_SPRITE;
            
            DrawCircle(renderer, posX, posY, rayon, color);
        }
    }



    
    for (unsigned int i = 0; i < 4; i++)
    {
        if(jeu.getTabPersonnageEnJeu()[i].getEnVie())
        {
            SDL_SetRenderDrawColor(renderer,0x00, 0xFF, 0x00, 0xFF);
            SDL_Color c;
            c.a = 0xFF;
            c.r = 0xFF;
            c.g = 0x00;
            c.b = 0x00;
            DrawCircle(renderer,jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().x* TAILLE_SPRITE,jeu.getTabPersonnageEnJeu()[i].getPerso().getPosition().y* TAILLE_SPRITE,jeu.getTabPersonnageEnJeu()[i].getPerso().getRayon()*TAILLE_SPRITE,c);
        }
    }
    // Ecrire un titre par dessus
    /*
    SDL_Rect positionTitre;
    positionTitre.x = jeu.getTerrain().getDimx()*TAILLE_SPRITE/4;
    positionTitre.y = 10;
    positionTitre.w = 150;
    positionTitre.h = 30;
    SDL_RenderCopy(renderer, font_im.getTexture(), nullptr, &positionTitre);
    */
    // Dessin du texte pour les litres d'essence
    std::string essenceText = "Litres d'essence :";
    SDL_Color textColor = {0, 0, 0};
    SDL_Surface* surfaceText = TTF_RenderText_Solid(font, essenceText.c_str(), textColor);
    SDL_Texture* textureText = SDL_CreateTextureFromSurface(renderer, surfaceText);
    int textWidth = 0, textHeight = 0;
    SDL_QueryTexture(textureText, NULL, NULL, &textWidth, &textHeight);
    SDL_Rect textRect = {15, jeu.getTerrain().getDimy() * TAILLE_SPRITE - textHeight - 5, textWidth, textHeight};
    SDL_RenderCopy(renderer, textureText, NULL, &textRect);

    // Dessin des rectangles pour chaque litre d'essence
    int startX = textRect.x + textRect.w + 5;
    int essence = jeu.getEssenceJ();
    for (int i = 0; i < essence; i++) {
        SDL_Rect rect = {startX + i * (10 + 1), textRect.y, 10, textHeight};
        SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
        SDL_RenderFillRect(renderer, &rect);
    }
    SDL_RenderPresent(renderer);
}

void JeuModeGraphique::affBoucleJeu() {
    SDL_Event events;
    bool quit = this->jeu.verifierFinPartie();
    int personnageActuel = 0;  // Suppose que personnageActuel est bien initialisé

    // tant que ce n'est pas la fin ...
    while (!quit) {
        // Traitement des actions automatiques du jeu
        jeu.actionAutomatique();

        // tant qu'il y a des évenements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events)) {
            if (events.type == SDL_QUIT) {
                quit = true; // Si l'utilisateur a cliqué sur la croix de fermeture
            } else if (events.type == SDL_KEYDOWN) {
                // Gestion des déplacements en fonction de la touche pressée
                switch (events.key.keysym.sym) {
                    case SDLK_0:
                    case SDLK_1:
                    case SDLK_2:
                    case SDLK_3:
                        personnageActuel = events.key.keysym.sym - SDLK_0;
                        break;
                    case SDLK_w: // Haut
                        jeu.actionClavier('h', personnageActuel);
                        break;
                    case SDLK_a: // Gauche
                        jeu.actionClavier('g', personnageActuel);
                        break;
                    case SDLK_s: // Bas
                        jeu.actionClavier('b', personnageActuel);
                        break;
                    case SDLK_d: // Droite
                        jeu.actionClavier('d', personnageActuel);
                        break;
                    case SDLK_f: // Placer personnage
                        jeu.placerPersonnageDansFile(personnageActuel);
                        break;
                    case SDLK_e: // Quitter
                        quit = true;
                        break;
                    default:
                        break;
                }
            }
        }

        // Rafraîchir l'affichage
        this->affGraphique();

        // Permutation des buffers
        SDL_RenderPresent(renderer);
    }
}



