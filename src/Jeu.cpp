#include "Jeu.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <cassert>

using namespace std::chrono;

Jeu::Jeu() : terrain(), nbEssenceJ(5), nbEssenceE(5), temps(0.0), estPartieFinie(false), compteurEssence(0) {
    debutPartie = steady_clock::now();
    for (int i = 0; i < 8; ++i) {
        Vecteur2D position(i < 4 ? 0.0 : terrain.getDimx() - 1, 1.0 + rand() % (terrain.getDimy() - 2));
        Particule p(position, 1+(rand()%10)/10);
        int vieInitiale = 20000 + rand() % 50; // Assurez-vous que cette valeur est correcte
        tabPersonnageEnJeu[i] = Personnage(p, vieInitiale, 1000+rand()%40, 1+rand()%5, rand()%7, true, false, i >= 4);
        std::cout << "Personnage " << i << " initialisé avec " << vieInitiale << " points de vie.\n";
    }

    for (int i = 0; i < 8; ++i) {
        Vecteur2D position(i < 4 ? 0.0 : terrain.getDimx() - 1, 1.0 + rand() % (terrain.getDimy() - 2));
        Particule p(position, 1+(rand()%10)/10);
        int vieInitiale = 20 + rand() % 50; // Assurez-vous que cette valeur est correcte
        Personnage nouveauPersonnage = Personnage(p, vieInitiale, 10+rand()%40, 1+rand()%5, rand()%7, true, false, i >= 4);
        
        // Enfiler les personnages dans la bonne file selon s'ils sont joueurs ou ennemis
        if (i < 4) {
            fileAttentePersonnage.push(nouveauPersonnage);
            std::cout << "Personnage joueur " << i << " initialisé avec " << vieInitiale << " points de vie et enfilé dans fileAttentePersonnage.\n";
        } else {
            fileAttentePersonnageEnnemi.push(nouveauPersonnage);
            std::cout << "Personnage ennemi " << i << " initialisé avec " << vieInitiale << " points de vie et enfilé dans fileAttentePersonnageEnnemi.\n";
        }
    }

}

Jeu::~Jeu() {}

void Jeu::actionAutomatique() {
    this->temps = duration_cast<duration<double>>(steady_clock::now() - debutPartie).count();
    std::cout << "                                                  Temps de jeu : " << this->temps << " secondes" << std::endl;
    compteurEssence++;

    if (verifierFinPartie()) {
        finirPartie();
        return;  // Assurez-vous que rien d'autre ne se passe après la fin de la partie
    }

    for (int i = 0; i < 8; ++i) {
        if (tabPersonnageEnJeu[i].getVie() > 0 && tabPersonnageEnJeu[i].getEstPlace()) {
            bool peutBouger = (tabPersonnageEnJeu[i].getEstJ() && this->nbEssenceJ > tabPersonnageEnJeu[i].getCout()) ||
                          (!tabPersonnageEnJeu[i].getEstJ() && this->nbEssenceE > tabPersonnageEnJeu[i].getCout());

            if (peutBouger) {
                genererCheminOptimalPourAttaquer(i);
                tabPersonnageEnJeu[i].deplacerPersonnageSurChemin(terrain);
                // Gestion des attaques uniquement sur tours ennemies
                for (unsigned int j = 0; j < terrain.getTabTour().size(); j++) {
                    if (terrain.getTabTour()[j].getEstJ() != tabPersonnageEnJeu[i].getEstJ() && estEnnemiProche(terrain.getTabTour()[j], tabPersonnageEnJeu[i])) {
                        tabPersonnageEnJeu[i].attaquer(terrain.getTabTour()[j]);
                        if (terrain.getTabTour()[j].getEnVie()) {
                            terrain.getTabTour()[j].attaquer(tabPersonnageEnJeu[i]);
                        }
                    }
                }
            }
        }
    }

    if (compteurEssence >= 30) {
        if (this->nbEssenceJ <= 10) nbEssenceJ++;
        if (this->nbEssenceE <= 10) nbEssenceE++;
        compteurEssence = 0;
    }
}





bool Jeu::verifierFinPartie() {
    bool principaleDetruiteJ = true;
    bool principaleDetruiteE = true;
    for (unsigned int i = 0; i < terrain.getTabTour().size(); i++) {
        if (terrain.getTabTour()[i].getEstJ() && terrain.getTabTour()[i].getPrincipale() && terrain.getTabTour()[i].getEnVie()) {
            principaleDetruiteJ = false;
            break;
        }
    }

    for (unsigned int i = 0; i < terrain.getTabTour().size(); i++) {
        if (!terrain.getTabTour()[i].getEstJ() && terrain.getTabTour()[i].getPrincipale() && terrain.getTabTour()[i].getEnVie()) {
            principaleDetruiteE = false;
            break;
        }
    }

    if (this->temps >= 180.0 || principaleDetruiteJ || principaleDetruiteE) {
        return true;
    } else {
        return false;
    }
}



void Jeu::finirPartie()
{
    cout <<"La partie est terminé" <<endl;
    exit(0); 
}

bool Jeu::estEnnemiProche(const Personnage& p1, const Personnage& p2)
{
    double distance = p1.getPerso().calculerDistance(p2.getPerso());
    double rayonAttaque = p1.getRayonAttaque();
    if(distance <= rayonAttaque)
        return true;
    return false;
}

bool Jeu::estEnnemiProche(const Tour& t, const Personnage& p)
{
    double distance = t.getTour().calculerDistance(p.getPerso());
    double rayonAttaque = t.getRayonAttaque();
    if(distance <= rayonAttaque)
        return true;
    return false;
}

Terrain Jeu::getTerrain() const
{
    return this->terrain;
}

void Jeu::actionClavier(const char& c, const unsigned int& i) {
    //std::cout << "Action clavier: " << c << ", Personnage: " << i << std::endl;
    // Récupérer les dimensions du terrain pour les calculs de limite
    int limiteVerticale = terrain.getDimy() / 2 + 2;

    Vecteur2D pos = tabPersonnageEnJeu[i].getPerso().getPosition();
    switch(c) {
        case 'g':
            // Déplacement à gauche toujours permis (ajoutez des limites si nécessaire)
            tabPersonnageEnJeu[i].gauche(terrain);
            break;
        case 'd':
            // Déplacement à droite toujours permis (ajoutez des limites si nécessaire)
            tabPersonnageEnJeu[i].droite(terrain);
            break;
        case 'h':
            // Permettre de monter seulement si le personnage est dans sa moitié de terrain
            if ((tabPersonnageEnJeu[i].getEstJ() && pos.y > 0) || (!tabPersonnageEnJeu[i].getEstJ() && pos.y > limiteVerticale)) {
                tabPersonnageEnJeu[i].haut(terrain);
            }
            break;
        case 'b':
            // Permettre de descendre seulement si le personnage est dans sa moitié de terrain
            if ((tabPersonnageEnJeu[i].getEstJ() && pos.y < limiteVerticale - 1) || (!tabPersonnageEnJeu[i].getEstJ() && pos.y < terrain.getDimy() - 1)) {
                tabPersonnageEnJeu[i].bas(terrain);
            }
            break;
        default:
            std::cout << "Touche non reconnue" << std::endl;
            break;
    }
}



void Jeu::genererCheminOptimalPourAttaquer(int indicePersonnage) {
    std::vector<char> deplacements;
    Tour* tourCible = nullptr;
    double distanceMinimale = std::numeric_limits<double>::max();
    bool estJ = tabPersonnageEnJeu[indicePersonnage].getEstJ();

    // Trouver la tour ennemie la plus proche encore en vie
    for (unsigned int i = 0; i < terrain.getTabTour().size(); i++) {
        // Assurez-vous de cibler uniquement les tours ennemies
        if (terrain.getTabTour()[i].getEstJ() != estJ && terrain.getTabTour()[i].getEnVie()) {
            double distance = tabPersonnageEnJeu[indicePersonnage].getPerso().calculerDistance(terrain.getTabTour()[i].getTour());
            if (distance < distanceMinimale) {
                distanceMinimale = distance;
                tourCible = &terrain.getTabTour()[i];
            }
        }
    }

    if (!tourCible) {
        std::cout << "Aucune tour ennemie en vie trouvée pour attaquer.\n";
        return; // Aucune tour ennemie en vie trouvée
    }

    Vecteur2D posActuelle = tabPersonnageEnJeu[indicePersonnage].getPerso().getPosition();

    // Générer le chemin vers la tour ennemie
    while (posActuelle.x != tourCible->getTour().getPosition().x) {
        if (posActuelle.x < tourCible->getTour().getPosition().x) {
            posActuelle.x++;
            deplacements.push_back('d'); // d pour droite
        } else {
            posActuelle.x--;
            deplacements.push_back('g'); // g pour gauche
        }
    }

    while (posActuelle.y != tourCible->getTour().getPosition().y) {
        if (posActuelle.y < tourCible->getTour().getPosition().y) {
            posActuelle.y++;
            deplacements.push_back('b'); // b pour bas
        } else {
            posActuelle.y--;
            deplacements.push_back('h'); // h pour haut
        }
    }

    tabPersonnageEnJeu[indicePersonnage].setChemin(deplacements);
}



const Personnage* Jeu::getTabPersonnageEnJeu() const {
    return tabPersonnageEnJeu;
}

void Jeu::placerPersonnageDansFile(const int& index) {
    if (!tabPersonnageEnJeu[index].getEstJ()) {
        if (this->nbEssenceE >= tabPersonnageEnJeu[index].getCout()) {
            tabPersonnageEnJeu[index].setEstPlace(true);
            this->nbEssenceE -= tabPersonnageEnJeu[index].getCout();
            fileAttentePersonnageEnnemi.push(tabPersonnageEnJeu[index]); // Enfile le personnage actuel

            if (!fileAttentePersonnageEnnemi.empty()) {
                Personnage personnageRemplacant = fileAttentePersonnageEnnemi.front(); // Obtient le personnage en tête
                fileAttentePersonnageEnnemi.pop(); // Défile le premier personnage
                tabPersonnageEnJeu[index] = personnageRemplacant; // Remplace le personnage actuel par le suivant dans la file
            }
        }
    }
    else {
        
    if (this->nbEssenceJ >= tabPersonnageEnJeu[index].getCout()) {
            tabPersonnageEnJeu[index].setEstPlace(true);
            this->nbEssenceJ -= tabPersonnageEnJeu[index].getCout();
            fileAttentePersonnage.push(tabPersonnageEnJeu[index]); // Enfile le personnage actuel

            if (!fileAttentePersonnage.empty()) {
                Personnage personnageRemplacant = fileAttentePersonnage.front(); // Obtient le personnage en tête
                fileAttentePersonnage.pop(); // Défile le premier personnage
                tabPersonnageEnJeu[index] = personnageRemplacant; // Remplace le personnage actuel par le suivant dans la file
            }
        }
    }
}


unsigned int Jeu::getEssenceJ() const
{
    return nbEssenceJ;
}

unsigned int Jeu::getEssenceE() const
{
    return nbEssenceE;
}

double Jeu::getTemps() const
{
    return temps;
}


void testRegressionJeu() {
    // Initialisation du jeu
    Jeu jeu;
    assert(jeu.getEssenceJ() == 5 && jeu.getEssenceE() == 5);  // Vérification des essences initiales
    cout << "Initialisation du jeu OK\n";

    // Vérification de la gestion du temps
    jeu.actionAutomatique();  // Simule un tick de jeu
    double tempsApresTick = jeu.getTemps();  // Récupère le temps après un tick
    assert(tempsApresTick > 0.0);  // Le temps doit avoir avancé
    cout << "Gestion du temps OK\n";

    // Vérification de la fin de partie
    bool fin = jeu.verifierFinPartie();  // Vérifie si la partie doit se terminer
    assert(!fin);  // La partie ne doit pas être finie juste après le début
    cout << "Vérification de la fin de partie OK\n";

    // Simuler une situation où une tour principale est détruite
    Terrain terrain = jeu.getTerrain();
    vector<Tour> tours = terrain.getTabTour();
    for (Tour& t : tours) {
        if (t.getPrincipale()) {
            t.recevoirDegats(t.getVie());  // Détruire la tour principale
        }
    }
    assert(jeu.verifierFinPartie() == true);  // La partie devrait être finie
    cout << "Test de fin de partie avec tour principale détruite OK\n";

    cout << "Tous les tests passés avec succès !\n";
}
