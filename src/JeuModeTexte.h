#ifndef _JEUMODETEXTE_H
#define _JEUMODETEXTE_H

#include "Jeu.h"
#include <cstdlib>
#include <time.h>

/**
 * @file JeuModeTexte.h
 * @brief Fichier d'en-tête déclarant la classe JeuModeTexte pour gérer l'affichage du jeu en mode texte.
 */

/**
 * @class JeuModeTexte
 * @brief Classe gérant l'affichage et les interactions du jeu en mode texte.
 * 
 * Cette classe encapsule la logique nécessaire pour exécuter et interagir avec le jeu dans une interface en mode texte, incluant des menus, des écrans de victoire ou de défaite, et l'interaction utilisateur via des commandes textuelles.
 */
class JeuModeTexte
{
private:
    Jeu jeu; /**< Instance du jeu gérant la logique de base. */

public:
    /**
     * @brief Constructeur par défaut de la classe JeuModeTexte.
     */
    JeuModeTexte();

    /**
     * @brief Destructeur de la classe JeuModeTexte.
     */
    ~JeuModeTexte();
    
    /**
     * @brief Fonction principale pour démarrer le jeu en mode texte et gérer la boucle principale.
     */
    void afftxtboucleJeu();
    
    /**
     * @brief Affiche le menu principal du jeu en mode texte.
     */
    void afficherMenuPrincipal();
    
    /**
     * @brief Affiche l'écran de victoire ou de défaite, selon l'issue du jeu.
     */
    void afficherGagnerouPerdu();
    
    /**
     * @brief Capture et traite les commandes entrées par l'utilisateur.
     */
    void saisirCommande();

    /**
     * @brief Efface la console ou la fenêtre de jeu avec un caractère spécifique.
     * @param c Caractère utilisé pour effacer l'écran (espace par défaut).
     */
    void clear(char c = ' ');
    
    /**
     * @brief Affiche un caractère à une position spécifique.
     * @param x Position horizontale.
     * @param y Position verticale.
     * @param c Caractère à afficher.
     */
    void print(int x, int y, char c);
    
    /**
     * @brief Affiche une chaîne de caractères à une position donnée.
     * @param x Position horizontale.
     * @param y Position verticale.
     * @param c Chaîne de caractères à afficher.
     */
    void print(int x, int y, char *c);
    
    /**
     * @brief Dessine un cadre ou une fenêtre de jeu à une position spécifique.
     * @param x Position horizontale du début de la fenêtre (0 par défaut).
     * @param y Position verticale du début de la fenêtre (0 par défaut).
     */
    void draw(int x = 0, int y = 0);

    /**
     * @brief Affiche les éléments textuels du jeu, tels que l'état du jeu ou des informations sur les personnages, dans une fenêtre spécifique.
     * @param win Référence à la fenêtre où le texte doit être affiché.
     * @param jeu Référence à l'instance de Jeu pour accéder aux informations à afficher.
     */
    void affTexte(WinTXT &win, const Jeu &jeu);
};

#endif
