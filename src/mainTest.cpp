#include <iostream>
#include "Vecteur2D.h"
#include "Particule.h"
#include "Obstacle.h"
#include "Personnage.h"
#include "Tour.h"
#include "Terrain.h"
#include "Jeu.h"

// Prototypes des fonctions testRegression (si elles ne sont pas déjà incluses dans les fichiers d'en-tête)
void testRegressionVecteur2D();
void testRegressionParticule();
void testRegressionObstacle();
void testRegressionPersonnage();
void testRegressionTour();
void testRegressionTerrain();
void testRegressionJeu();

int main() {
    std::cout << "Début des tests de régression...\n";

    testRegressionVecteur2D();
    testRegressionParticule();
    testRegressionObstacle();
    testRegressionPersonnage();
    testRegressionTour();
    testRegressionTerrain();
    testRegressionJeu();

    std::cout << "Tous les tests de régression ont été exécutés.\n";

    return 0;
}
