# Bataille des Rois

## Informations du Projet
- **Étudiants** : Safwan BEN ALI 12103356, Justin CAMILE 12108765
- **Identifiant du Projet** : 12103356_12108765
- **URL du Projet sur la Forge** : [Forge Université Lyon 1](https://forge.univ-lyon1.fr/12103356_12108765/bataille-des-rois)

## Manuel

### Compilation
Pour compiler le jeu, utilisez les commandes suivantes dans le répertoire racine du projet :

- make all # Compile toutes les cibles nécessaires

### Exécution
Pour exécuter le jeu après la compilation :

- ./bin/graphique # Lance la version graphique du jeu
- ./bin/texte # Lance la version texte du jeu

### Règles du Jeu
Bataille des Rois est un jeu de stratégie où vous devez utiliser vos personnages pour attaquer et détruire la tour principale de l'adversaire tout en défendant la vôtre. Chaque personnage a des caractéristiques spécifiques telles que la vie, les dégâts, le rayon d'attaque, et le coût en essence pour le placer sur le terrain.

### Commandes
- **z/q/s/d** : Déplace le personnage sélectionné vers le haut/gauche/bas/droite.
- **f** : Place le personnage sélectionné sur le terrain.
- **0 à 3** : Sélectionne le personnage correspondant pour le déplacement ou l'attaque.

### Fonctionnalités
- Deux modes de jeu : graphique et texte.
- Interaction en temps réel avec les personnages.
- Système de gestion de l'essence pour placer les personnages.
- Affichage dynamique des éléments du jeu : personnages, tours, et obstacles.
- Détecte et annonce le gagnant de la partie.

## Organisation de l'Archive
- **/bin/** : Contient les exécutables du jeu.
- **/src/** : Contient les fichiers d'en-tête `.h` et fichiers source `.cpp` du jeu.
- **/obj/** : Contient les fichiers objet générés lors de la compilation.
- **/data/** : Contient les ressources graphiques et les polices utilisées dans le jeu.
- **makefile** : Fichier utilisé pour compiler le projet.
- **README.md** : Ce fichier contenant les informations sur le projet.